package cn.zxw.prompt;

import cn.zxw.login.entity.AppModel;
import cn.zxw.mina.ClientHandler;
import cn.zxw.utils.CommenUtils;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

import java.net.URL;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;

public class PromptController  implements Initializable {
    public  static AppModel model = new AppModel();
    private static PromptController instance;
    @FXML
    private Label companyOrUserId;
    @FXML
    private Label xtitle;
    @FXML
    private Label xtype;

    private String id;



    public static void setText(String text)
    {
        model.setText(text);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Map<String, String> userData = ClientHandler.p_map;
        String type = userData.get("type");
        StringBuilder sb = new StringBuilder();
        sb.append("【");
        sb.append(type);
        sb.append("】");
        String title = userData.get("title");
        String sendUserOrg = userData.get("sendUserOrg");
        id = userData.get("id");
       /* url = userData.get("url");*/
        xtitle.setText(CommenUtils.splitTitle(false,title));
        xtype.setText(sb.toString());
        companyOrUserId.setText(sendUserOrg);
    }


    //查看详情
    public void seeDetils(){
        try {
            System.out.print("点击开始");
            Properties properties = CommenUtils.getProperties("plugin.properties");
            if(properties!=null){
                String notic_ip = properties.get("notic_ip").toString();
                CommenUtils.browse(id,notic_ip,ClientHandler.token);
            }
            System.out.print(properties+"点击结束");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //关闭窗体
    @FXML
    public void close() {
        PromptApp.primaryStage.close();
    }


}
