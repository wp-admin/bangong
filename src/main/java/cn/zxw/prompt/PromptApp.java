package cn.zxw.prompt;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class PromptApp {

    public static Stage primaryStage;
    /**
     * 功能描述: 消息窗体工厂<br>
     * @Return: javafx.stage.Stage
     * @Author: zxw
     * @Date: 2020/6/29 11:39
     */
    public Stage start() throws Exception{
        Rectangle2D bounds = Screen.getPrimary().getVisualBounds();
        primaryStage = new Stage();
        primaryStage.initStyle(StageStyle.TRANSPARENT);
        primaryStage.setX(bounds.getMaxX() - 500);
        primaryStage.setY(bounds.getMaxY() - 190);
        primaryStage=primaryStage;
        return primaryStage;
    }
}
