package cn.zxw.functionset;

import cn.zxw.login.LoginApp;
import cn.zxw.login.entity.AppModel;
import cn.zxw.mina.ClientHandler;
import cn.zxw.prompt.PromptController;
import cn.zxw.utils.CommenUtils;
import cn.zxw.utils.StringUtil;
import com.jfoenix.controls.JFXCheckBox;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;

import javax.swing.*;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;

/**
 * 功能描述: 功能设置<br>
 * @Author: zxw
 * @Date: 2020/7/10 21:19
 */
public class FunctionController  implements Initializable {
    private FunctionApp application;
    public  static AppModel model = new AppModel();
    private static PromptController instance;
    public static String soundR = "0";
    public static String areatR = "0";
    public static String taskR = "0";
    public static String startR = "0";
    @FXML
    public  CheckBox soundReminder;//声音
    @FXML
    public  CheckBox alertReminder;//弹框
    @FXML
    public  CheckBox taskReminder;//任务栏
    @FXML
    public  CheckBox startUp;//开机启动

    public static void setText(String text)
    {
        model.setText(text);
    }

    public void setApp(FunctionApp application) {
        this.application = application;
    }
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //初始化选择框
        Properties properties = CommenUtils.getProperties("setting.properties");
        soundR =(String)properties.get("soundR");
        areatR =(String)properties.get("areatR");
        taskR = (String)properties.get("taskR");
        startR =(String) properties.get("startR");
        if(soundR.equals("1")){
            soundReminder.setSelected(true);
        }
        if(areatR.equals("1")){
            alertReminder.setSelected(true);
        }
        if(taskR.equals("1")){
            taskReminder.setSelected(true);
        }
        if(startR.equals("1")){
            startUp.setSelected(true);
        }

        //用于监听选择框
        soundReminder.selectedProperty().addListener(new ChangeListener<Boolean>() {
            public void changed(ObservableValue<? extends Boolean> ov,
                                Boolean old_val, Boolean new_val) {
                System.out.println(soundReminder.isSelected());
                FunctionController.soundR =soundReminder.isSelected() ?"1" : "0";//1是选中  0是未选中
            }
        });
        alertReminder.selectedProperty().addListener(new ChangeListener<Boolean>() {
            public void changed(ObservableValue<? extends Boolean> ov,
                                Boolean old_val, Boolean new_val) {
                System.out.println(alertReminder.isSelected());
                areatR =alertReminder.isSelected() ?"1" : "0";
            }
        });
        taskReminder.selectedProperty().addListener(new ChangeListener<Boolean>() {
            public void changed(ObservableValue<? extends Boolean> ov,
                                Boolean old_val, Boolean new_val) {
                System.out.println(taskReminder.isSelected());
                taskR =taskReminder.isSelected() ?"1" : "0";
            }
        });
        startUp.selectedProperty().addListener(new ChangeListener<Boolean>() {
            public void changed(ObservableValue<? extends Boolean> ov,
                                Boolean old_val, Boolean new_val) {
                System.out.println(startUp.isSelected());
                startR =startUp.isSelected() ?"1" : "0";
            }
        });
    }




}
