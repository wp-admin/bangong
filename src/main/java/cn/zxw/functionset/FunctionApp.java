package cn.zxw.functionset;

import cn.zxw.login.LoginApp;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.InputStream;

import static cn.zxw.login.LoginApp.stage;

public class FunctionApp {

    /**
     * 功能描述: 系统设置<br>
     * @Return: javafx.stage.Stage
     * @Author: zxw
     * @Date: 2020/6/29 11:39
     */
    public  Stage start() throws Exception{
        Stage primaryStage = new Stage();
        Scene m_scene = new Scene(FXMLLoader.load(getClass().getResource("/functionset/Functionset.fxml")));
        primaryStage.initStyle(StageStyle.TRANSPARENT);
        primaryStage.setScene(m_scene);
        return primaryStage;
    }



}
