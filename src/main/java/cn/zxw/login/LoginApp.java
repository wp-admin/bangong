package cn.zxw.login;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LoginApp extends Application {

    public static Stage stage;
    public static double x1;
    public static double y1;
    public static double x_stage;
    public static double y_stage;


    @Override
    public  void start(Stage primaryStage) {
        stage = primaryStage;
        stage.setTitle("办公助手");
        stage.initStyle(StageStyle.TRANSPARENT);
        stage.getIcons().add(new Image("/image/ICON.png"));
        gotoLogin();
        stage.show();
    }

    public void gotoLogin(){
        try {
            LoginController login = (LoginController) replaceSceneContent("/login/Login.fxml");
            login.setApp(this);
        } catch (Exception ex) {
            Logger.getLogger(LoginApp.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    public void userLogin(String account,String password){
        Check check = new Check();
        boolean b = check.checkReturn(account, password);
        if(!b) {
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    try {
                        LoginController.showalertInfo("连接服务器失败！！");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    public static Initializable replaceSceneContent(String fxml) throws Exception {
        FXMLLoader loader = new FXMLLoader();
        InputStream in = LoginApp.class.getResourceAsStream(fxml);
        loader.setBuilderFactory(new JavaFXBuilderFactory());
        loader.setLocation(LoginApp.class.getResource(fxml));
        BorderPane page;
        try {
            page = (BorderPane) loader.load(in);
        } finally {
            in.close();
        }
        Scene scene = new Scene(page);
        stage.setScene(scene);
        stage.sizeToScene();
        scene.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override public void handle(MouseEvent m) {
                //计算
                stage.setX(x_stage + m.getScreenX() - x1);
                stage.setY(y_stage + m.getScreenY() - y1);
            }
        });
        scene.setOnDragEntered(null);
        scene.setOnMousePressed(new EventHandler<MouseEvent>() {

            @Override public void handle(MouseEvent m) {
                //按下鼠标后，记录当前鼠标的坐标
                x1 =m.getScreenX();
                y1 =m.getScreenY();
                x_stage = stage.getX();
                y_stage = stage.getY();
            }
        });
        return (Initializable) loader.getController();
    }


    public static void main(String[] args) {
        launch(args);
    }

}
