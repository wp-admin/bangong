package cn.zxw.main;

import cn.zxw.common.TraySystem;
import cn.zxw.login.Check;
import cn.zxw.login.LoginApp;
import cn.zxw.mina.ClientHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

import java.awt.*;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * @author zxw
 */
public class MainController implements Initializable{
    @FXML
    private Label userName;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.userName.setText(ClientHandler.userName);
    }

    //关闭窗体
    @FXML
    public void close() {
        Check check = new Check();
        check.quit();
        SystemTray.getSystemTray().remove(TraySystem.trayIcon);
        LoginApp.stage.close();
    }


    //窗体最小化操作
    @FXML
    public void min() {
        boolean connect = Check.connect;
        if(connect){
            if(!TraySystem.min_status){
                TraySystem.getInstance().enableTray(LoginApp.stage);
            }
            LoginApp.stage.setIconified(true);
            //LoginApp.stage.close();
        }
    }



}
