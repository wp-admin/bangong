package cn.zxw.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.net.URL;

/**
 * @author zxw
 */
public class StringUtil {
    private Logger logger = LoggerFactory.getLogger(StringUtil.class);

    private StringUtil() {

    }

    public static String getRootPath(URL url) {
        String fileUrl = url.getFile();
        int pos = fileUrl.indexOf('!');

        if (-1 == pos) {
            return fileUrl;
        }

        return fileUrl.substring(5, pos);
    }



    public static String trimExtension(String name) {
        int pos = name.lastIndexOf('.');
        if (-1 != pos) {
            return name.substring(0, pos);
        }

        return name;
    }

    /**
     * @param uri
     * @return
     */
    public static String trimURI(String uri) {
        String trimmed = uri.substring(1);
        int splashIndex = trimmed.indexOf('/');

        return trimmed.substring(splashIndex);
    }





}