package cn.zxw.utils;

import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;

public class TestScheduledService extends ScheduledService<Void>  {

    @Override
    protected Task<Void> createTask() {
        return new Task<Void>() {
            @Override
            protected Void call() {
                System.out.println("ScheduledService_Task begin");
                return null;
            }
        };
    }

}
