package cn.zxw.mina;

import org.apache.mina.core.filterchain.IoFilter;
import org.apache.mina.core.future.ConnectFuture;
import org.apache.mina.core.service.IoConnector;
import org.apache.mina.core.service.IoHandler;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.textline.LineDelimiter;
import org.apache.mina.filter.codec.textline.TextLineCodecFactory;
import org.apache.mina.filter.logging.LoggingFilter;
import org.apache.mina.filter.logging.MdcInjectionFilter;
import org.apache.mina.filter.ssl.SslFilter;
import org.apache.mina.transport.socket.nio.NioSocketConnector;

import javax.net.ssl.SSLContext;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;


/**
 * @author zxw
 */
public class ChatClientSupport {

    public static IoSession session;
    public static boolean cont_status;
    public static ChatClientSupport chatClientSupport;
    public Map<String, IoSession> map = new HashMap<String, IoSession>();

    public ChatClientSupport(){
        chatClientSupport=this;
    }

    public boolean connect(NioSocketConnector connector, InetSocketAddress  inetSocketAddress) {
        if (session != null && session.isConnected()) {
            throw new IllegalStateException(
                    "已经建立连接！！！");
        }
        try{
            connector.setConnectTimeout(3000);// 设置超时时间
            // 设置过滤器(编码和解码)
           /* connector.getFilterChain().addLast("codec", new ProtocolCodecFilter(
                    new TextLineCodecFactory(Charset.forName("UTF-8"),
                            LineDelimiter.WINDOWS.getValue(),
                            LineDelimiter.WINDOWS.getValue())));*/
            TextLineCodecFactory textLineCodecFactory = new TextLineCodecFactory(Charset.forName("UTF-8"), LineDelimiter.WINDOWS.getValue(), LineDelimiter.WINDOWS.getValue());
            textLineCodecFactory.setDecoderMaxLineLength(6048);
            ProtocolCodecFilter protocolCodecFilter = new ProtocolCodecFilter(textLineCodecFactory);
            connector.getFilterChain().addLast("codec",protocolCodecFilter );
            // 业务处理
            connector.setHandler(new ClientHandler());
            // 设置session属性,获取服务端连接
            ConnectFuture future = connector.connect(inetSocketAddress);
            future.awaitUninterruptibly();// 等待我们的连接
            session = future.getSession();
            cont_status=true;
            return cont_status;
        } catch (Exception e) {
            cont_status=false;
            return cont_status;
        }
    }


    public void broadcast(String message) {
        session.write(message);
    }


    //退出
    public void quit() {
        if (session != null) {
            if (session.isConnected()) {
                session.getCloseFuture().awaitUninterruptibly();
            }
            session.close(true);
        }
    }

}
