package cn.zxw.common;


import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class Msg extends JFrame
{
    private static final long serialVersionUID = 1L;

    public static void main(String[] args)
    {
        EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                try
                {
                    Msg frame = new Msg();
                    frame.setVisible(true);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });
    }

    public Msg()
    {
        setBounds(100, 100, 260, 120);
        setResizable(false);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(null);

        JLabel label = new JLabel("您有新的消息");
        label.setBounds(77, 30, 100, 15);
        getContentPane().add(label);

    }
}