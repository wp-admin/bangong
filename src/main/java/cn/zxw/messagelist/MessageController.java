package cn.zxw.messagelist;

import cn.zxw.login.entity.AppModel;
import cn.zxw.login.entity.BaseEntity;
import cn.zxw.mina.ClientHandler;
import cn.zxw.utils.CommenUtils;
import cn.zxw.utils.DateUtil;
import cn.zxw.utils.StringUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

import java.io.InputStream;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static cn.zxw.mina.ChatClientSupport.session;
import static cn.zxw.mina.ClientHandler.messageStage;
import static cn.zxw.mina.ClientHandler.token;

/**
 * 功能描述:初始化消息列表 <br>
 * @Author: zxw
 * @Date: 2020/7/3 9:25
 */
public class MessageController extends StackPane
implements Initializable  {
    public  static AppModel model = new AppModel();
    private static MessageController instance;
    @FXML
    private AnchorPane liststrip;
    @FXML
    private Label msgnum;

    /**
     * listciew数据
     */
    private ObservableList<String> dataList = FXCollections.observableArrayList();

    public static void setText(String text)
    {
        model.setText(text);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)  {
        Date date = new Date();
        SimpleDateFormat simple = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        JSONArray listData = ClientHandler.listData;
        if(listData.size()>0){
            int j=0;
            Iterator<Object> it = listData.iterator();
            String size = String.valueOf(listData.size());
            StringBuilder sb_num = new StringBuilder();
            sb_num.append("(");
            sb_num.append(size);
            sb_num.append(")");
            msgnum.setText(sb_num.toString());
            while (it.hasNext()) {
                JSONObject ob = (JSONObject) it.next();
                try {
                    date = simple.parse(ob.get("time").toString());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                System.out.println(DateUtil.format(date));
                Pane pane = new Pane();
                pane.setPrefHeight(102.0);
                pane.setPrefWidth(262.0);
                pane.setLayoutX(1.0);
                pane.setLayoutY(j);
                j+=100;
                ObservableList<Node> children = pane.getChildren();
                StringBuilder sb = new StringBuilder();
                sb.append("【");
                sb.append(ob.get("type"));
                sb.append("】");
                Label lable_type = new Label(sb.toString());
                lable_type.setLayoutX(11.0);
                lable_type.setLayoutY(4.0);
                lable_type.setTextFill(Color.web("#514e4e"));

                Label lable_date = new Label(ob.get("time").toString());
                lable_date.setLayoutX(115.0);
                lable_date.setLayoutY(4.0);
                lable_date.setTextFill(Color.web("#8d7b7b"));

                Label lable_title = new Label(CommenUtils.splitTitle(true,ob.get("title").toString()));
                lable_title.setLayoutX(14.0);
                lable_title.setLayoutY(30.0);
                lable_title.setTextFill(Color.web("#0004f5"));
                lable_title.setFont(Font.font(13.0));
                Label lable_user = new Label(ob.get("sendUserOrg").toString());
                lable_user.setLayoutX(20.0);
                lable_user.setLayoutY(61.0);
                lable_user.setTextFill(Color.web("#685c5c"));

                Pane pane1 = new Pane();
                pane1.setLayoutX(14.0);
                pane1.setLayoutY(7.0);
                pane1.setPrefHeight(90.0);
                pane1.setPrefWidth(240.0);
                pane1.setStyle("-fx-background-color: E9E9E9;");

                //pane的点击事件
                pane1.addEventFilter(MouseEvent.MOUSE_CLICKED,new EventHandler<MouseEvent>() {
                    public void handle(MouseEvent event){
                        try {
                            Properties properties = CommenUtils.getProperties("plugin.properties");
                            String notic_ip = properties.get("notic_ip").toString();
                            CommenUtils.browse(ob.get("id").toString(),notic_ip, token);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    };
                });

                ObservableList<Node> children1 = pane1.getChildren();
                children1.add(lable_type);
                children1.add(lable_date);
                children1.add(lable_title);
                children1.add(lable_user);
                children.add(pane1);
                liststrip.getChildren().add(pane) ;
            }
        }else {
            Image image = new Image("/image/无数据.png");
            ImageView imageView = new ImageView();
            imageView.setX(60);
            imageView.setY(20);
            imageView.setImage(image);
            liststrip.getChildren().add(imageView) ;
        }

    }




    /**
     * 清空消息列表按钮
     * @param event
     */
    @FXML
    protected void clear(ActionEvent event) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                BaseEntity baseEntity = new BaseEntity();
                baseEntity.setSign("emptyNotic");//清空列表
                baseEntity.setToken(token);
                String jsonStr = JSON.toJSONString(baseEntity);
                session.write(jsonStr);
                messageStage.hide();
            }
        });
    }




}
