package cn.zxw.messagelist;

import cn.zxw.common.TraySystem;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/*
 * 功能描述: 列表框<br>
 * @Author: zxw
 * @Date: 2020/7/1 16:46
 */
public class MessageListApp {


    public Stage start() throws Exception{
        Stage primaryStage = new Stage();
        primaryStage.initStyle(StageStyle.TRANSPARENT);
        primaryStage.setX(TraySystem.X_position-300);
        primaryStage.setY(TraySystem.y_position-360);
        return primaryStage;
    }
}
