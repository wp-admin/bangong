package cn.zxw.messagelist;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;


public class Message extends Pane {
    String date;
    Label type=new Label();
    Label title=new Label();
    Label user=new Label();

    public Message( String date,String type,String title,String user){
        this.date=date;
        this.title.setText(title);
        this.type.setText(type);
        this.user.setText(user);

        Pane pane = new Pane();
            pane.setPrefHeight(102.0);
            pane.setPrefWidth(262.0);
            pane.setLayoutX(1.0);
            pane.setLayoutY(0.0);
            ObservableList<Node> children = pane.getChildren();
            Label lable_type = new Label("【日程提醒】");
            lable_type.setLayoutX(11.0);
            lable_type.setLayoutY(4.0);
            lable_type.setTextFill(Color.web("#514e4e"));

            Label lable_date = new Label("46分钟前");
            lable_date.setLayoutX(158.0);
            lable_date.setLayoutY(3.0);
            lable_date.setTextFill(Color.web("#8d7b7b"));

            Label lable_title = new Label("王毅：“一带一路不是债务”陷...");
            lable_title.setLayoutX(14.0);
            lable_title.setLayoutY(30.0);
            lable_title.setTextFill(Color.web("#0004f5"));
            lable_title.setFont(Font.font(13.0));

            Label lable_user = new Label("北京市公安局办公室数据中心");
            lable_user.setLayoutX(20.0);
            lable_user.setLayoutY(61.0);
            lable_user.setTextFill(Color.web("#685c5c"));

            Pane pane1 = new Pane();
            pane1.setLayoutX(14.0);
            pane1.setLayoutY(7.0);
            pane1.setPrefHeight(90.0);
            pane1.setPrefWidth(240.0);
            pane1.setStyle("-fx-background-color: E9E9E9;");
            ObservableList<Node> children1 = pane1.getChildren();
            children1.add(lable_type);
            children1.add(lable_date);
            children1.add(lable_title);
            children1.add(lable_user);
            children.add(pane1);
    }

}
