
#### 介绍及说明
基于javaFx+mina办公助手客户端，本项目分客户端和服务端，客户端基于javaFx+socket实现。服务端基于mina实现。

#### 预览图片
![输入图片说明](https://images.gitee.com/uploads/images/2021/0605/112217_d3cb1f49_1390937.jpeg "1.jpg")
###
![输入图片说明](https://images.gitee.com/uploads/images/2021/0605/112234_4c9f6f1d_1390937.jpeg "2.jpg")
###
![输入图片说明](https://images.gitee.com/uploads/images/2021/0605/112249_233104b1_1390937.jpeg "3.jpg")

#### 目前实现功能
<!-- wp:paragraph -->
<p><strong>功能：</strong>1.实现socket登录</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>2、心跳检索</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>3、消息接收闪动</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>4、接收消息通知</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>5、消息列表</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>6、消息数量统计</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>7、列表消息跳转</p>
<!-- /wp:paragraph -->

#### 使用说明

1.  把后端代码拷贝到项目当中集成。
2.  客户端代码使用idea编译
3.  注意修改前后端的ip地址



如需要支持Linux国产环境请见——C++版



